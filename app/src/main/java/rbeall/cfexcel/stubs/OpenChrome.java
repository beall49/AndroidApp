package rbeall.cfexcel.stubs;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.customtabs.CustomTabsIntent;

import rbeall.cfexcel.R;
import rbeall.cfexcel.chrome.CustomTabActivityHelper;
import rbeall.cfexcel.utils.Utilities;

public class OpenChrome {
	public static void newTab(Activity _activity, Uri uri) {
		Context _context = _activity.getApplicationContext();

		CustomTabsIntent.Builder customTabsIntent = new CustomTabsIntent.Builder();
		customTabsIntent.setToolbarColor(new Utilities(_activity)
						.getColor(R.color.primary))
						.setShowTitle(true)
						.setStartAnimations(_context, R.anim.abc_fade_in, R.anim.abc_fade_out);

		CustomTabActivityHelper.openCustomTab(_activity,
				customTabsIntent.build(), uri, (activity, uri1) -> {
				Intent intent = new Intent(Intent.ACTION_VIEW, uri1)
					.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					_context.startActivity(intent);
				}
			);
	}
}
