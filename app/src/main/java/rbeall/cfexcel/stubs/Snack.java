package rbeall.cfexcel.stubs;

import android.content.Context;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.view.View;

import rbeall.cfexcel.utils.Utilities;

public class Snack {
	public void show (Context context, View view, @StringRes int text, int length){
		Snackbar.make(view, new Utilities(context).getString(text), length).show();
	}
}
