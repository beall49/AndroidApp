package rbeall.cfexcel.stubs;

import android.content.Context;
import android.os.IBinder;
import android.view.inputmethod.InputMethodManager;


public class HideKeyboard {
    public HideKeyboard(Context ctx, IBinder token) {
        InputMethodManager mgr = (InputMethodManager) ctx.getSystemService(Context.INPUT_METHOD_SERVICE);
        mgr.hideSoftInputFromWindow(token, 0);
    }
}