package rbeall.cfexcel.views;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;
import io.realm.Realm;
import io.realm.RealmResults;
import rbeall.cfexcel.R;
import rbeall.cfexcel.api.WorkOutTable;
import rbeall.cfexcel.stubs.OpenChrome;
import rbeall.cfexcel.utils.Utilities;

public class SingleWorkout extends Fragment {
	@Bind(R.id.txtDescription) TextView txtDesc;
	@Bind(R.id.imgView) ImageView imgView;
	@Bind(R.id.scrollView) NestedScrollView scrollView;
	@Bind(R.id.singleLayout) CoordinatorLayout singleLayout;
	@Bind(R.id.app_bar_layout) AppBarLayout appBarLayout;
	@Bind(R.id.collapsing_toolbar) CollapsingToolbarLayout collapsingToolbarLayout;
	@Bind(R.id.toolbar) Toolbar toolbar;

	Utilities utils;
	Realm realm;
	Context context;
	WorkOutTable workOutTable;

	@Override public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		context = getActivity();
		realm = Realm.getDefaultInstance();
		utils = new Utilities(context);

		Intent intent = getActivity().getIntent();
		String workOutId = intent.getStringExtra(utils.getString(R.string.WORKOUT_ID));

		if (!WorkOutTable.isValidRecord(workOutId)) {
			getActivity().onBackPressed();
		}

		View view = inflater.inflate(R.layout.fragment_single_workout, container, false);
		ButterKnife.bind(this, view);


		collapsingToolbarLayout.setExpandedTitleColor(getResources().getColor(R.color.tranny));


		txtDesc.setOnLongClickListener(v -> {
			if (txtDesc != null && txtDesc.getTag() != null) {
				new OpenChrome().newTab(getActivity(), Uri.parse(txtDesc.getTag().toString()));
			}
			return false;
		});

		workOutTable = WorkOutTable.getRecords(utils.getString(R.string.WORKOUT_ID), workOutId).get(0);

		txtDesc.setText(String.format(Locale.US, "%s \n\n%s",
				workOutTable.getTitle(),
				workOutTable.getPostText()
		));

		txtDesc.setTag(workOutTable.getUrl());
		toolbar.setTitle(workOutTable.getTitle());


		Picasso.with(context).load(workOutTable.getImage()).into(imgView);

		return view;
	}

	@Override public void onDestroyView() {
		super.onDestroyView();
		ButterKnife.unbind(this);
	}

	public SingleWorkout() {
	}
}