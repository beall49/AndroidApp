package rbeall.cfexcel.views;
import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import java.text.DecimalFormat;
import java.util.List;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import rbeall.cfexcel.R;
import rbeall.cfexcel.stubs.HideKeyboard;
import rbeall.cfexcel.utils.Utilities;

public class Calculator extends Fragment {
	private Utilities utils;
	private final DecimalFormat df = new DecimalFormat("###.#");
	private Double answerValue = 0.0;
	private String weight;

	@Bind(R.id.etAnswer) EditText etAnswer;
	@Bind(R.id.etEntry) EditText etEntry;
	@Bind({R.id.bKilo, R.id.bLBS}) List<Button> buttons;
	@Bind({R.id.etAnswer, R.id.etEntry}) List<EditText> textViews;

	@Override public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_calculator, container, false);
		ButterKnife.bind(this, view);
		utils = new Utilities(getContext());
		etEntry.requestFocus();
		return view;
	}

	@OnClick({R.id.bLBS, R.id.bKilo}) public void onClick(View v) {
		Double entryValue = Double.parseDouble(etEntry.getText().toString());
		this.getAnswer(entryValue, (v.getId() == R.id.bKilo));
		etAnswer.setText(String.format(Locale.US, "%.1f%s", this.answerValue, this.weight));
		new HideKeyboard(getActivity(), etAnswer.getWindowToken());
		this.resetEntryTxt();
	}

	@OnTextChanged(value = R.id.etEntry, callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED) public void textChanged() {
		boolean bool = Utilities.doubleCheck(etEntry.getText().toString());
		ButterKnife.apply(buttons, ENABLED, bool);
	}

	@Override public void onDestroyView() {
		super.onDestroyView();
		ButterKnife.unbind(this);
	}

	@Override public void onAttach(Activity activity) {
		super.onAttach(activity);
		mListener = (OnFragmentInteractionListener) activity;
	}

	@Override public void onDetach() {
		super.onDetach();
		mListener = null;
	}

	@Override public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	private void getAnswer(Double entry, boolean isKilos) {
		this.weight = (isKilos ? utils.getString(R.string.KILOS) : utils.getString(R.string.POUNDS));
		this.answerValue = Double.parseDouble(df.format((isKilos ? entry / 2.2 : entry * 2.2)));
	}

	public void resetEntryTxt() {
		ButterKnife.apply(textViews, FOCUS, false);
		ButterKnife.apply(textViews, FOCUS, true);
		this.answerValue = 0.0;
	}

	private OnFragmentInteractionListener mListener;

	public interface OnFragmentInteractionListener {
		void onFragmentInteraction(Uri uri);
	}

	public Calculator() {
	}

	public static Calculator newInstance() {
		return new Calculator();
	}

	/*
		BUTTERKNIFE SETTERS
	 */
	static final ButterKnife.Setter<View, Boolean> FOCUS = (view, value, index) -> {
		view.setFocusableInTouchMode(value);
		view.setFocusable(value);
	};

	static final ButterKnife.Setter<View, Boolean> ENABLED = (view, value, index) -> view.setEnabled(value);




}