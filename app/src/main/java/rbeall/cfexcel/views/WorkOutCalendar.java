package rbeall.cfexcel.views;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;

import java.util.List;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;
import io.realm.Realm;
import io.realm.RealmResults;
import rbeall.cfexcel.R;
import rbeall.cfexcel.WorkoutActivity;
import rbeall.cfexcel.api.WorkOutObservables;
import rbeall.cfexcel.api.WorkOutTable;
import rbeall.cfexcel.stubs.Snack;
import rbeall.cfexcel.utils.Utilities;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action0;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by rbeall on 3/18/2016.
 */
public class WorkOutCalendar extends Fragment implements
											  com.prolificinteractive.materialcalendarview.OnDateSelectedListener {

	View view;
	private Subscription sub;
	private Utilities utils;
	private String workOutId = "0";
	@Bind({R.id.wod_calendar}) MaterialCalendarView wod_calendar;
	@Bind({R.id.progressBar}) ProgressBar progressBar;

	@Override public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		view = inflater.inflate(R.layout.fragment_calendar, container, false);
		ButterKnife.bind(this, view);
		utils = new Utilities(getContext());
		setUpCalendar();
		return view;
	}

	@Override public void onDateSelected(@NonNull final MaterialCalendarView view, @NonNull CalendarDay date, boolean selected) {
		String entryDate = String.format(Locale.US, "%02d%02d%02d",
				date.getYear(),
				date.getMonth() + 1,
				date.getDay()
		);

		RealmResults<WorkOutTable> results = WorkOutTable.getRecords(utils.getString(R.string.CLEAN_DATE), entryDate);

		if (results.size() > 0) {
			startSingleWorkout(results.get(0).getPostId());
			return;
		}

		progressBar.setVisibility(view.VISIBLE);
		sub = new WorkOutObservables(getContext())
				.observableSingleDate(entryDate)
				.observeOn(AndroidSchedulers.mainThread())
				.subscribeOn(Schedulers.io())
				.subscribe(onNextAction, onErrorAction, onCompleteAction);
	}

	public Action0 onCompleteAction = new Action0() {
		@Override public void call() {
			progressBar.setVisibility(view.GONE);
			startSingleWorkout (workOutId);
		}
	};

	public Action1<Throwable> onErrorAction = new Action1<Throwable>() {
		@Override public void call(Throwable e) {
			new Snack().show(getContext(), view, R.string.SKWAT, Snackbar.LENGTH_LONG);
			utils.logEr(e.toString());
			progressBar.setVisibility(View.GONE);
		}
	};

	public Action1<List<WorkOutTable>> onNextAction = new Action1<List<WorkOutTable>>() {
		@Override public void call(List<WorkOutTable> workOutTbl) {
			workOutId = workOutTbl.get(0).getPostId();
			WorkOutTable.addRecord(workOutTbl);
		}
	};


	public static WorkOutCalendar newInstance() {
		return new WorkOutCalendar();
	}

	public WorkOutCalendar() {
	}

	private OnFragmentInteractionListener mListener;
	public interface OnFragmentInteractionListener {
		void onFragmentInteraction(Uri uri);
	}

	public void startSingleWorkout(String workOutId) {
		startActivity(new Intent(getContext(),
				WorkoutActivity.class
		)
				.putExtra(utils.getString(R.string.WORKOUT_ID), workOutId));
	}

	public void setUpCalendar(){
		wod_calendar.setFirstDayOfWeek(2);
		wod_calendar.setOnDateChangedListener(this);
		wod_calendar.setSelectionColor(utils.getColor(R.color.accent));
		wod_calendar.setSelectedDate(CalendarDay.today());
		wod_calendar.setArrowColor(utils.getColor(R.color.colorAccent));
		progressBar.setVisibility(View.GONE);
	}

	@Override public void onAttach(Context context) {
		super.onAttach(context);
		if (context instanceof OnFragmentInteractionListener)
			mListener = (OnFragmentInteractionListener) context;
	}

	@Override public void onDetach() {
		super.onDetach();
		mListener = null;
	}

	@Override public void onDestroyView() {
		super.onDestroyView();
		if (sub != null && !sub.isUnsubscribed())
			sub.unsubscribe();
		ButterKnife.unbind(this);
	}

	@Override public void onDestroy() {
		super.onDestroy();
		if (sub != null && !sub.isUnsubscribed())
			sub.unsubscribe();
		ButterKnife.unbind(this);
	}

	@Override public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
}