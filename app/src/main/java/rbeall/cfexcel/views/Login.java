package rbeall.cfexcel.views;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rbeall.cfexcel.R;
import rbeall.cfexcel.stubs.Snack;
import rbeall.cfexcel.utils.Utilities;

public class Login extends Fragment {
	private static int RESULT_LOAD_IMAGE = 1;
	private Utilities utils;
	private Context context;

	@Bind(R.id.imgView) ImageView imgQR;
	@Bind(R.id.btnFab) FloatingActionButton fab;

	@OnClick({R.id.imgView, R.id.btnFab}) public void getImage() {
		Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
		i.setType("image/*").setAction(Intent.ACTION_GET_CONTENT);
		startActivityForResult(Intent.createChooser(i, "Select Picture"), RESULT_LOAD_IMAGE);
	}

	@Override public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		context = getContext();
		utils = new Utilities(context);

		View view = inflater.inflate(R.layout.fragment_login, container, false);
		ButterKnife.bind(this, view);

		if (utils.isNotDefault()) {
			imgQR.setBackground(utils.returnImage());
			return view;
		}

		new Snack().show(context,
				view,
				R.string.SNACK_BAR_QR, Snackbar.LENGTH_INDEFINITE);
		return view;
	}

	@Override public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (requestCode == RESULT_LOAD_IMAGE && resultCode == Activity.RESULT_OK && data != null) {

			Uri selectedImage = data.getData();
			try {
				Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), selectedImage);

				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
				utils.setFilePath(Base64.encodeToString(baos.toByteArray(), Base64.DEFAULT));
				Picasso.with(context).load(selectedImage).into(imgQR);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@Override public void onAttach(Activity activity) {
		super.onAttach(activity);
		mListener = (OnFragmentInteractionListener) activity;
	}

	@Override public void onDetach() {
		super.onDetach();
		mListener = null;
	}

	@Override public void onDestroyView() {
		super.onDestroyView();
		ButterKnife.unbind(this);
	}

	@Override public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	private OnFragmentInteractionListener mListener;
	public interface OnFragmentInteractionListener {
		void onFragmentInteraction(Uri uri);
	}

	public Login() {
	}

	public static Login newInstance() {
		return new Login();
	}
}