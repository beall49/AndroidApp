package rbeall.cfexcel.views;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnItemClick;
import io.realm.Realm;
import rbeall.cfexcel.R;
import rbeall.cfexcel.WorkoutActivity;
import rbeall.cfexcel.api.WorkOutTable;
import rbeall.cfexcel.stubs.Snack;
import rbeall.cfexcel.api.WorkOutObservables;
import rbeall.cfexcel.utils.WorkOutAdapter;
import rbeall.cfexcel.utils.Utilities;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action0;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class Workouts extends Fragment {

	@Bind(R.id.lstRSS) ListView lstRSS;
	@Bind(R.id.swipe_listview) SwipeRefreshLayout swipeLayout;

	WorkOutAdapter workOutAdapter;
	private Subscription sub;
	private View view;
	private Utilities utils;
	private boolean refreshing;

	/*
		when a card is clicked it opens the
		big workout window
	 */
	@OnItemClick(R.id.lstRSS) public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		int finalRadius = (int) Math.hypot(view.getWidth()/2, view.getHeight()/2);
		TextView txtTitle = (TextView) view.findViewById(R.id.txtTitle);
		String workOutId = (txtTitle.getTag().toString());

		Intent intent = new Intent(getActivity(), WorkoutActivity.class)
				.putExtra(utils.getString(R.string.WORKOUT_ID), workOutId);

		ViewAnimationUtils.createCircularReveal(
				view,
				view.getWidth()/2,
				view.getHeight()/2,
				0,
				finalRadius
		).setDuration(350).start();
		startActivity(intent);
	}

	/*
		create view
	*/
	@Override public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		view = inflater.inflate(R.layout.fragment_workouts, container, false);
		ButterKnife.bind(this, view);

		utils = new Utilities(getContext());
		swipeLayout.setColorSchemeResources(R.color.colorAccent, R.color.colorAccent2, R.color.md_amber_900);
		swipeLayout.setOnRefreshListener(() -> refreshArticles());

		workOutAdapter = new WorkOutAdapter(getActivity());
		lstRSS.setAdapter(workOutAdapter);
		if (WorkOutTable.getRecordCount() == 0 || workOutAdapter.isEmpty()) {
			Snackbar.make(view, utils.getString(R.string.pull_to_refresh), Snackbar.LENGTH_LONG)
					.show();
		}
		return view;
	}

	public void refreshArticles() {
		//if it's already refreshing exit
		if (refreshing)
			return;

		if (swipeLayout.isLaidOut() && !swipeLayout.isRefreshing())
			swipeLayout.setRefreshing(true);

		sub = new WorkOutObservables(getContext())
				.observableEntries()
				.subscribeOn(Schedulers.io())
				.observeOn(AndroidSchedulers.mainThread())
				.filter(workOuts -> workOuts != null)
				.subscribe(onNextAction, onErrorAction, onCompleteAction);
		workOutAdapter.notifyDataSetChanged();
	}

	public Action0 onCompleteAction = new Action0() {
		@Override public void call() {
			if (lstRSS != null){
				workOutAdapter.notifyDataSetChanged();
				lstRSS.setAdapter(workOutAdapter);
				swipeLayout.setRefreshing(false);
				refreshing = false;
			}
		}
	};

	public Action1<Throwable> onErrorAction = new Action1<Throwable>() {
		@Override public void call(Throwable e) {
			new Snack().show (getContext(),
					view,
					R.string.SNACK_BAD_SERVICE, Snackbar.LENGTH_LONG);

			swipeLayout.setRefreshing(false);
			utils.logEr(e.toString());
			refreshing = false;
		}
	};

	public Action1<List<WorkOutTable>> onNextAction = new Action1<List<WorkOutTable>>() {
		@Override public void call(List<WorkOutTable> workOutTbl) {
			WorkOutTable.addRecord(workOutTbl);
			workOutAdapter.notifyDataSetChanged();
			swipeLayout.setRefreshing(false);
		}
	};

	@Override public void setUserVisibleHint(boolean isVisibleToUser) {
		super.setUserVisibleHint(isVisibleToUser);
		if (isVisibleToUser) {
			if (!(workOutAdapter == null)) {
				workOutAdapter.notifyDataSetChanged();
			}
		}
	}

	@Override public void onDestroy() {
		super.onDestroy();
		if (sub != null && !sub.isUnsubscribed())
			sub.unsubscribe();
		ButterKnife.unbind(this);
	}

	@Override public void onResume() {
		super.onResume();
	}

	@Override public void onPause() {
		super.onPause();
		if (sub != null && !sub.isUnsubscribed())
			sub.unsubscribe();
	}

	@Override public void onDestroyView() {
		super.onDestroyView();
		ButterKnife.unbind(this);
	}

	@Override public void onAttach(Context context) {
		super.onAttach(context);
		mListener = (OnFragmentInteractionListener) context;
	}

	@Override public void onDetach() {
		super.onDetach();
		mListener = null;
	}

	public Workouts() {
	}

	private OnFragmentInteractionListener mListener;
	public interface OnFragmentInteractionListener {
		void onFragmentInteraction(Uri uri);
	}

	public static Workouts newInstance() {
		return new Workouts();
	}

}