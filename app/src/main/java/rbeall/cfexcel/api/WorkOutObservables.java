package rbeall.cfexcel.api;
import android.app.Service;
import android.content.Context;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

import io.realm.RealmObject;
import rbeall.cfexcel.R;
import rbeall.cfexcel.utils.Utilities;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Observable;

public class WorkOutObservables {
	Context context;
	Utilities utils;
	WorkOutEndPoints api;
	public WorkOutObservables(Context _context) {
		context = _context;
		utils = new Utilities(context);
		api = 	 new Retrofit.Builder()
				.baseUrl(utils.getString(R.string.API_BASE))
				.addCallAdapterFactory(RxJavaCallAdapterFactory.create())
				.addConverterFactory(GsonConverterFactory.create(getGson()))
				.build()
				.create(WorkOutEndPoints.class);
	}

	public Observable<List<WorkOutTable>> observableEntries() {
		return api.getEntries();
	}

	public Observable<List<WorkOutTable>> observableSingleDate(String incomingDate) {
		return api.getSingleDate(incomingDate);
	}

	public Gson getGson() {
		return new GsonBuilder()
				.setExclusionStrategies(new ExclusionStrategy() {
					@Override
					public boolean shouldSkipField(FieldAttributes f) {
						return f.getDeclaringClass().equals(RealmObject.class);
					}

					@Override
					public boolean shouldSkipClass(Class<?> clazz) {
						return false;
					}
				})
				.create();
	}
}
