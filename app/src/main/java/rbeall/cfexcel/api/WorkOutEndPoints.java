package rbeall.cfexcel.api;
import java.util.List;

import retrofit2.http.GET;
import retrofit2.http.Path;
import rx.Observable;

public interface WorkOutEndPoints {

	@GET("entries/15") Observable<List<WorkOutTable>> getEntries();

	@GET("date/{incoming_date}") Observable<List<WorkOutTable>>  getSingleDate(@Path("incoming_date") String incoming_date);
}
