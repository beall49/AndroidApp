package rbeall.cfexcel.api;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmResults;
import io.realm.Sort;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

public class WorkOutTable extends RealmObject {
	private static final String SCHEDULE_URL = "http://www.crossfitexcel.com/schedule";


	@Required @PrimaryKey
	private String current_id;

	private String id;
	private String title;
	private String image;
	private String url;
	private String post_text;
	private String clean_date;
	private String date_add;

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getUrl() {
		if (this.url.length() > 0)
			return this.url;
		return SCHEDULE_URL;
	}

	public String getPostText() {
		return (this.post_text.replace("\n", "\n\n").replace("then,","then:\n")) + "\n";
	}

	public String getPostId() {
		return current_id;
	}

	public String getTitle() {
		DateFormat incoming = new SimpleDateFormat("EEEE MMM d, y", Locale.US);
		DateFormat outgoing = new SimpleDateFormat("EEEE M.dd.yyyy ", Locale.US);
		String pattern = "(\\d)+(st|nd|rd|th)";
		Matcher m = Pattern.compile(pattern).matcher(title);
		if (m.find() && m.groupCount()==2){
			String superDate = m.group(0).replace(m.group(2), "");

			try{
				return outgoing.format(incoming.parse(title.replace(m.group(0), superDate) ));
			} catch (Exception e){
				e.printStackTrace();
			}
		}
		return title;
	}

	public String getCleanDate() {
		return clean_date;
	}

	public String getDateAdd() {
		return date_add;
	}

	public  boolean checkUrl(){
		return (url != null) && url.length()>0 ;
	}

	public static int getRecordCount(){
		Realm realm = Realm.getDefaultInstance();
		RealmResults<WorkOutTable> results = realm.where(WorkOutTable.class).findAll();
		return results.size();
	}

	public  static RealmResults<WorkOutTable> getAllRecords(){
		Realm realm = Realm.getDefaultInstance();
		return realm.where(WorkOutTable.class).findAllSorted("current_id", Sort.DESCENDING);
	}

	public  static RealmResults<WorkOutTable> getRecords(String column, String value){
		Realm realm = Realm.getDefaultInstance();
		return realm.where(WorkOutTable.class).equalTo(column, value).findAll();
	}

	public static boolean isValidRecord(String id){
		Realm realm = Realm.getDefaultInstance();
		RealmResults<WorkOutTable> results = realm.where(WorkOutTable.class).equalTo("current_id", id).findAll();
		return (results.size()>0);
	}

	public static void addRecord(List<WorkOutTable> workOuts){
		if (workOuts != null) {
			Realm realm = Realm.getDefaultInstance();
			realm.beginTransaction();
			realm.copyToRealmOrUpdate(workOuts);
			realm.commitTransaction();
			realm.close();
		}
	}

}
