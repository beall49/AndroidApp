package rbeall.cfexcel;
import android.app.Application;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import rbeall.cfexcel.api.WorkOutEndPoints;
import rbeall.cfexcel.api.WorkOutTable;

//setting default config for realm, gotta be a better way
public class MyApplication extends Application {
	@Override public void onCreate() {
		super.onCreate();
		Realm.setDefaultConfiguration(new RealmConfiguration.Builder(getApplicationContext()).build());
		/*
			Realm realm = Realm.getDefaultInstance();
			realm = Realm.getInstance(getApplicationContext());
			realm.beginTransaction();
			realm.clear(WorkOutTable.class);
			realm.commitTransaction();
		*/
	}
}
