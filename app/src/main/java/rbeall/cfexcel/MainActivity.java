package rbeall.cfexcel;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ImageSpan;
import android.view.Menu;
import android.view.MenuItem;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import io.realm.Realm;
import rbeall.cfexcel.api.WorkOutObservables;
import rbeall.cfexcel.api.WorkOutTable;
import rbeall.cfexcel.stubs.OpenChrome;
import rbeall.cfexcel.utils.TabAdapter;
import rbeall.cfexcel.utils.Utilities;
import rbeall.cfexcel.utils.WorkOutAdapter;
import rbeall.cfexcel.views.Calculator;
import rbeall.cfexcel.views.Login;
import rbeall.cfexcel.views.WorkOutCalendar;
import rbeall.cfexcel.views.Workouts;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity implements
													Calculator.OnFragmentInteractionListener,
													Login.OnFragmentInteractionListener,
													WorkOutCalendar.OnFragmentInteractionListener,
													Workouts.OnFragmentInteractionListener,
													TabLayout.OnTabSelectedListener {

	FragmentManager fm = getSupportFragmentManager();
	public TabAdapter tabAdapter;

	private Context context;
	private Utilities utils;
	private Subscription sub;
	private Realm realm;

	@Bind(R.id.container) ViewPager viewPager;
	@Bind(R.id.sliding_tabs) TabLayout tabLayout;
	@Bind(R.id.toolbar) Toolbar toolbar;

	@Override protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		getWindow().setBackgroundDrawable(null);
		ButterKnife.bind(this);


		context = getApplicationContext();
		utils = new Utilities(context);
		tabAdapter = new TabAdapter(fm, MainActivity.this);
		realm = Realm.getDefaultInstance();

		viewPager.setAdapter(tabAdapter);
		viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

		tabLayout.setupWithViewPager(viewPager);
		tabLayout.setOnTabSelectedListener(this);

		toolbar.setTitle(utils.getString(R.string.TITLE0));
		setIcons(0);
		setSupportActionBar(toolbar);
		new WorkOutAdapter(context).notifyDataSetChanged();
	}

	//check if new posts
	private void checkForUpdates() {
		sub = new WorkOutObservables(context)
				.observableEntries()
				.subscribeOn(Schedulers.io())
				.observeOn(AndroidSchedulers.mainThread())
				.filter(workOuts -> workOuts != null)
				//if there's no err, whatever
				.subscribe(workOuts -> WorkOutTable.addRecord(workOuts), e -> {});

		//notify list adapter
		new WorkOutAdapter(context).notifyDataSetChanged();
	}

	//set icons instead of titles
	public void setIcons(int position) {
		for (int x = 0; x < tabAdapter.getCount(); x++) {
			Drawable image = ContextCompat.getDrawable(context,
					this.getResources()
						.obtainTypedArray(R.array.icons)
						.getResourceId(x, 0)
			);

			if (x == position) //if Im the current selection
				image.setColorFilter(utils.getColor(Utilities.SELECTED_ICON), PorterDuff.Mode.MULTIPLY);

//			image.setBounds(0, 0, 48, 48);
			try {
				tabLayout.getTabAt(x).setIcon(image);
			} catch (NullPointerException e) {
				e.printStackTrace();
			}
		}
	}

	//change color of selected tab
	@Override public void onTabSelected(TabLayout.Tab tab) {
		int position = tab.getPosition();
		viewPager.setCurrentItem(position);
		setIcons(position);
		toolbar.setTitle(this.getResources()
							 .obtainTypedArray(R.array.titles)
							 .getString(position));
	}

	//unsubscribe from rx
	@Override protected void onDestroy() {
		super.onDestroy();
		if (sub != null && !sub.isUnsubscribed())
			sub.unsubscribe();
	}

	//catch weird err when restarting
	@Override protected void onResume() {
		super.onResume();
		try {
			utils = new Utilities(getApplicationContext());

			if (realm.allObjects(WorkOutTable.class).size()==0)
				tabLayout.getTabAt(0).select();

		} catch (NullPointerException e) {
			e.printStackTrace();
		}
	}

	//open chrome to schedule when clicking menu icon
	@Override public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.schedule) {
			String schedule = utils.getString(R.string.SCHEDULE_URL);
			Uri uri = Uri.parse(schedule);
			try {
				new OpenChrome().newTab(this, uri);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return super.onOptionsItemSelected(item);
	}


	@Override public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return super.onCreateOptionsMenu(menu);
	}


	@Override public void onFragmentInteraction(Uri uri) {	}
	@Override public void onTabUnselected(TabLayout.Tab tab) {	}
	@Override public void onTabReselected(TabLayout.Tab tab) {	}
}
