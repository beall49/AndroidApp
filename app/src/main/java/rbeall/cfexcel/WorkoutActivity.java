package rbeall.cfexcel;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import butterknife.Bind;
import butterknife.ButterKnife;
import rbeall.cfexcel.views.SingleWorkout;

public class WorkoutActivity extends AppCompatActivity {
	@Bind(R.id.toolbar) Toolbar toolbar;

	@Override protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_single_workout);
		ButterKnife.bind(this);

		getWindow().setBackgroundDrawable(null);
		setSupportActionBar(toolbar);

		try {
			getSupportActionBar().setDisplayHomeAsUpEnabled(true);
			getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
			getSupportActionBar().setTitle(null);
		} catch (NullPointerException e) {
			e.printStackTrace();
		}

		if (savedInstanceState == null)
			getFragmentManager()
					.beginTransaction()
					.add(R.id.container, new SingleWorkout())
					.commit();
	}

	@Override public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == android.R.id.home)
			onBackPressed();
		return true;
	}
}
