package rbeall.cfexcel.utils;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ImageSpan;

import rbeall.cfexcel.R;
import rbeall.cfexcel.views.Calculator;
import rbeall.cfexcel.views.Login;
import rbeall.cfexcel.views.WorkOutCalendar;
import rbeall.cfexcel.views.Workouts;

public class TabAdapter extends android.support.v4.app.FragmentPagerAdapter {
	final int PAGE_COUNT = 4;
	private Resources resources;

	private Context context;
	private SpannableString sb = new SpannableString(" ");

	public TabAdapter(FragmentManager fm, Context context) {
		super(fm);
		this.context = context;
		resources = context.getResources();
	}

	@Override public Fragment getItem(int position) {
		if (position == 1) return Login.newInstance();
		if (position == 2) return Calculator.newInstance();
		if (position == 3) return WorkOutCalendar.newInstance();
		return Workouts.newInstance();
	}

	@Override public void restoreState(Parcelable state, ClassLoader loader) {
		super.restoreState(state, loader);
	}

	@Override public CharSequence getPageTitle(int position) {
		Drawable image = ContextCompat.getDrawable(context,
				resources.obtainTypedArray(R.array.icons).getResourceId(position, 0)
		);


		if (position == 0)
			image.setColorFilter(context.getResources().getColor(Utilities.SELECTED_ICON), PorterDuff.Mode.MULTIPLY);

		ImageSpan imageSpan = new ImageSpan(image, ImageSpan.ALIGN_BOTTOM);
		sb.setSpan(imageSpan, 0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		return sb;
	}

	@Override	public int getItemPosition(Object object) {
		return POSITION_NONE;
	}

	@Override 	public int getCount() {
		return PAGE_COUNT;
	}
}
