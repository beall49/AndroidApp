package rbeall.cfexcel.utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.support.annotation.ColorRes;
import android.support.annotation.StringRes;
import android.util.Base64;
import android.util.Log;

import java.util.regex.Pattern;

import rbeall.cfexcel.R;

public class Utilities {
	//colors
	public static final int SELECTED_ICON = R.color.colorAccent;


	//local resources

	private String FILE_PATH = "";
	public static final String DEFAULT_LOGO = "drawable://logored";
	private static final String APP_SHARED_PREFS = Utilities.class.getSimpleName();


	private SharedPreferences preferences;
	private SharedPreferences.Editor editor;

	private Context context;



	public Utilities(Context context) {
		this.context = context;
		this.preferences = context.getApplicationContext().getSharedPreferences(APP_SHARED_PREFS, Activity.MODE_PRIVATE);
		this.editor = preferences.edit();
		this.editor.apply();
	}

	public String getString(@StringRes int resource) {
		return context.getResources().getString(resource);
	}

	public int getColor(@ColorRes int resource) {
		return context.getResources().getColor(resource);
	}

	public String getFilePath() {
		return preferences.getString(FILE_PATH, DEFAULT_LOGO);
	}

	public void setFilePath(String text) {
		editor.putString(FILE_PATH, text);
		editor.commit();
	}


	public void logEr(String message) {
		Log.e(this.getString(R.string.TAG), message);
	}

	public boolean isNotDefault() {
		return !(DEFAULT_LOGO.equals(getFilePath()));
	}

	public BitmapDrawable returnImage() {
		byte[] imageAsBytes = Base64.decode(getFilePath(), 0);
		return new BitmapDrawable(context.getResources(), BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length));
	}

	/*
		crazy regex to validate doubles
	*/
	public static boolean doubleCheck(String myEntry) {
		final String Digits = "(\\p{Digit}+)",
				HexDigits = "(\\p{XDigit}+)",
				Exp = "[eE][+-]?" + Digits;

		final String fpRegex = (
				"[\\x00-\\x20]*" +  // Optional leading "whitespace"
						"[+-]?(" + // Optional sign character
						"NaN|" +           // "NaN" string
						"Infinity|" +      // "Infinity" string

						// Digits ._opt Digits_opt ExponentPart_opt FloatTypeSuffix_opt
						"(((" + Digits + "(\\.)?(" + Digits + "?)(" + Exp + ")?)|" +

						// . Digits ExponentPart_opt FloatTypeSuffix_opt
						"(\\.(" + Digits + ")(" + Exp + ")?)|" +

						// Hexadecimal strings
						"((" +
						// 0[xX] HexDigits ._opt BinaryExponent FloatTypeSuffix_opt
						"(0[xX]" + HexDigits + "(\\.)?)|" +

						// 0[xX] HexDigits_opt . HexDigits BinaryExponent FloatTypeSuffix_opt
						"(0[xX]" + HexDigits + "?(\\.)" + HexDigits + ")" +

						")[pP][+-]?" + Digits + "))" +
						"[fFdD]?))" +
						"[\\x00-\\x20]*");// Optional trailing "whitespace"

		return (Pattern.matches(fpRegex, myEntry));
	}
}
