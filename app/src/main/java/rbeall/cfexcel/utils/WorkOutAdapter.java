package rbeall.cfexcel.utils;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import butterknife.Bind;
import butterknife.ButterKnife;
import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;
import rbeall.cfexcel.R;
import rbeall.cfexcel.api.WorkOutTable;
import rbeall.cfexcel.stubs.SquareImageView;

public class WorkOutAdapter extends BaseAdapter {

	private Context context;
	WorkOutTable workOutTbl;
	Realm realm;
	Utilities utils;
	/*
		sets the data for each
		workout card
	*/
	@Override public View getView(int position, View view, ViewGroup parent) {
		ViewHolder holder;
		RealmResults<WorkOutTable> results = WorkOutTable.getAllRecords();

		if (results.size()>0) {
			workOutTbl = results.get(position);

			if (view != null) {
				holder = (ViewHolder) view.getTag();
			} else {
				view = LayoutInflater.from(context).inflate(R.layout.cards, parent, false);
				holder = new ViewHolder(view);
				view.setTag(holder);
			}

			if (workOutTbl.checkUrl()) {
				Picasso.with(context).load(workOutTbl.getImage()).fit().into(holder.imgView);
				holder.txtTitle.setText(workOutTbl.getTitle());
				holder.txtTitle.setTag(workOutTbl.getPostId());
			}
		}
		if (this.isEmpty()) {
			Snackbar.make(view, utils.getString(R.string.pull_to_refresh), Snackbar.LENGTH_INDEFINITE)
					.show();
		}

		return view;
	}

	public WorkOutAdapter(Context c) {
		super();
		context = c;
	}

	@Override public Object getItem(int i) {
		return i;
	}

	public long getItemId(int position) {
		return position;
	}

	public int getCount() {
		return WorkOutTable.getRecordCount();
	}

	static class ViewHolder {
		@Bind(R.id.txtTitle) TextView txtTitle;
		@Bind(R.id.imgCardView) ImageView imgView;


		public ViewHolder(View view) {
			ButterKnife.bind(this, view);
		}
	}
}